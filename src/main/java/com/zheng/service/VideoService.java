package com.zheng.service;

import com.zheng.pojo.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Z2823
* @description 针对表【video】的数据库操作Service
* @createDate 2024-10-10 19:11:32
*/
public interface VideoService extends IService<Video> {

}
