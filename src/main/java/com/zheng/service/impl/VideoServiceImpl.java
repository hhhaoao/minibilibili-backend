package com.zheng.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheng.pojo.Video;
import com.zheng.service.VideoService;
import com.zheng.mapper.VideoMapper;
import org.springframework.stereotype.Service;

/**
* @author Z2823
* @description 针对表【video】的数据库操作Service实现
* @createDate 2024-10-10 19:11:32
*/
@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video>
    implements VideoService{

}




