package com.zheng.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @TableName video
 */
@TableName(value ="video")
@Data
public class Video implements Serializable {
    private String videoId;

    private String userId;

    private String videOname;

    private Double duration;

    private String coverUrl;

    private String videoUrl;

    private Integer status;

    private Date createTime;

    private static final long serialVersionUID = 1L;
}