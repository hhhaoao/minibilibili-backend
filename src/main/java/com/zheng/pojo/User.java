package com.zheng.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @TableName user
 */
@TableName(value ="user")
@Data
public class User implements Serializable {
    @TableId(value = "user_id")
    private String userId;

    private String phone;

    private String password;

    private String nickname;

    private String avatar;

    private Integer sex;

    private Date createDate;

    private static final long serialVersionUID = 1L;
}