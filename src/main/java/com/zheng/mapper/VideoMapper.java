package com.zheng.mapper;

import com.zheng.pojo.Video;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Z2823
* @description 针对表【video】的数据库操作Mapper
* @createDate 2024-10-10 19:11:32
* @Entity com.zheng.pojo.Video
*/
public interface VideoMapper extends BaseMapper<Video> {

}




